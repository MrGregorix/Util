package me.mrgregorix.util.function.cache;

import java.util.Map;

/**
 * Cache that stores values in the map
 *
 * @param <K> key type
 * @param <V> value type
 */
public class MapBasedCache<K, V> implements Cache<K, V>
{
    private final Map<K, V> map;

    public MapBasedCache(final Map<K, V> map)
    {
        this.map = map;
    }

    @Override
    public boolean has(final K key)
    {
        return this.map.containsKey(key);
    }

    @Override
    public V get(final K key)
    {
        return this.map.get(key);
    }

    @Override
    public V set(final K key, final V value)
    {
        return this.map.put(key, value);
    }

    public Map<K, V> getMap()
    {
        return this.map;
    }
}

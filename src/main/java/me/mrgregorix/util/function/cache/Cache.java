package me.mrgregorix.util.function.cache;

import java.util.function.Function;

/**
 * Cache is a type of data structures similar to map if the given key
 * exists, returns his value, but if it isn't puts the supplied value
 * to the map with given key
 *
 * @param <K> key type
 * @param <V> value type
 */
public interface Cache<K, V>
{
    /**
     * @param key key
     *
     * @return true if value for given key exists
     */
    boolean has(K key);

    /**
     * @param key key
     *
     * @return get value of given key, or null if it isn't exists
     */
    V get(K key);

    /**
     * Adds or changes value in the cache
     *
     * @param key   key
     * @param value value
     *
     * @return previous value on given key if was, or null if it's new
     * key
     */
    V set(K key, V value);

    /**
     * Get value of if it isn't exists, adds it using the supplier
     *
     * @param key      key
     * @param supplier missing value supplier
     *
     * @return value
     */
    default V getCached(K key, Function<K, V> supplier)
    {
        if (this.has(key))
        {
            return this.get(key);
        }
        else
        {
            V value = supplier.apply(key);
            if (value == null)
            {
                return null;
            }
            else
            {
                this.set(key, value);
                return value;
            }
        }
    }
}

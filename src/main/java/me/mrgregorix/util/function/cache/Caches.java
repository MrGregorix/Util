package me.mrgregorix.util.function.cache;

import java.util.Map;

import com.google.common.collect.Maps;

/**
 * Simple cache creating utils
 */
public final class Caches
{
    private Caches()
    {
    }

    public static <K, V> MapBasedCache<K, V> newMapBasedCache(final Map<K, V> map)
    {
        return new MapBasedCache<>(map);
    }

    public static <K, V> MapBasedCache<K, V> newHashMapBasedCache()
    {
        return new MapBasedCache<>(Maps.newHashMap());
    }
}

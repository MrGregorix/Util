package me.mrgregorix.util.function.box;

/**
 * Container for a primitive type {@code float}
 */
public class FloatBox
{
    protected float value;

    public float get()
    {
        return value;
    }

    public void set(final float value)
    {
        this.value = value;
    }
}

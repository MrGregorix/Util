package me.mrgregorix.util.function.box;

/**
 * Container for a primitive type {@code char}
 */
public class CharBox
{
    protected char value;

    public char get()
    {
        return value;
    }

    public void set(final char value)
    {
        this.value = value;
    }
}

package me.mrgregorix.util.function.box;

/**
 * Container for a primitive type {@code double}
 */
public class DoubleBox
{
    protected double value;

    public double get()
    {
        return value;
    }

    public void set(final double value)
    {
        this.value = value;
    }
}

package me.mrgregorix.util.function.box;

/**
 * Container for a value
 *
 * @param <T> container type
 */
public class Box<T>
{
    protected T value;

    public T get()
    {
        return value;
    }

    public void set(final T value)
    {
        this.value = value;
    }
}

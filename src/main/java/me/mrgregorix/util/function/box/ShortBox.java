package me.mrgregorix.util.function.box;

/**
 * Container for a primitive type {@code short}
 */
public class ShortBox
{
    protected short value;

    public short get()
    {
        return value;
    }

    public void set(final short value)
    {
        this.value = value;
    }
}

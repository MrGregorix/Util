package me.mrgregorix.util.function.box;

/**
 * Container for a primitive type {@code int}
 */
public class IntBox
{
    protected int value;

    public int get()
    {
        return value;
    }

    public void set(final int value)
    {
        this.value = value;
    }
}

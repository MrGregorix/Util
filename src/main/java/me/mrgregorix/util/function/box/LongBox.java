package me.mrgregorix.util.function.box;

/**
 * Container for a primitive type {@code long}
 */
public class LongBox
{
    protected long value;

    public long get()
    {
        return value;
    }

    public void set(final long value)
    {
        this.value = value;
    }
}

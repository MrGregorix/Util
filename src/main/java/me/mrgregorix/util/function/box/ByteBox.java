package me.mrgregorix.util.function.box;

/**
 * Container for a primitive type {@code byte}
 */
public class ByteBox
{
    protected byte value;

    public byte get()
    {
        return value;
    }

    public void set(final byte value)
    {
        this.value = value;
    }
}

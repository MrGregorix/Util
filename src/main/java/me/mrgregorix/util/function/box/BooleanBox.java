package me.mrgregorix.util.function.box;

/**
 * Container for a primitive type {@code boolean}
 */
public class BooleanBox
{
    protected boolean value;

    public boolean get()
    {
        return value;
    }

    public void set(final boolean value)
    {
        this.value = value;
    }
}

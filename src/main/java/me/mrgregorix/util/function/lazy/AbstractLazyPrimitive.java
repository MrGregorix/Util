package me.mrgregorix.util.function.lazy;

import me.mrgregorix.util.reflect.ReflectClass;
import me.mrgregorix.util.reflect.ReflectField;

import org.apache.commons.lang.Validate;

/**
 * Primitive lazy value
 *
 * @param <W> wrapping class
 */
public abstract class AbstractLazyPrimitive<W> extends AbstractLazyValue<W>
{
    private final Class<?> primitiveClass;

    protected AbstractLazyPrimitive(final Class<W> wrapperClass, final Class<?> primitiveClass)
    {
        Validate.isTrue(primitiveClass.isPrimitive(), "invalid class");
        final ReflectField<?> field = ReflectClass.forClass(wrapperClass).getField("TYPE");
        Validate.isTrue(field != null && field.getStatic() == primitiveClass, "Invalid wrapper");
        this.primitiveClass = primitiveClass;
    }

    @Override
    protected W init()
    {
        final W value = this.init_();
        Validate.isTrue(value != null && this.primitiveClass.isInstance(value), "invalid primitive");
        return value;
    }

    protected abstract W init_();

    @Override
    protected boolean reset_()
    {
        return true;
    }
}

package me.mrgregorix.util.function.lazy.primitive;

import java.util.function.BooleanSupplier;

import me.mrgregorix.util.function.lazy.AbstractLazyPrimitive;

/**
 * Provided lazy initialization for primitive value {@code boolean}
 */
public class LazyBoolean extends AbstractLazyPrimitive<Boolean>
{
    private final BooleanSupplier supplier;

    public LazyBoolean(final BooleanSupplier supplier)
    {
        super(Boolean.class, boolean.class);
        this.supplier = supplier;
    }

    /**
     * Get the value from cache, if the value is not cached, it will
     * be got from the supplier
     */
    public boolean getAsBoolean()
    {
        return this.get_();
    }

    @Override
    protected Boolean init_()
    {
        return supplier.getAsBoolean();
    }
}

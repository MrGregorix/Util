package me.mrgregorix.util.function.lazy.primitive;

import java.util.function.LongSupplier;

import me.mrgregorix.util.function.lazy.AbstractLazyPrimitive;

/**
 * Provided lazy initialization for primitive value {@code long}
 */
public class LazyLong extends AbstractLazyPrimitive<Long>
{
    private final LongSupplier supplier;

    public LazyLong(final LongSupplier supplier)
    {
        super(Long.class, long.class);
        this.supplier = supplier;
    }

    /**
     * Get the value from cache, if the value is not cached, it will
     * be got from the supplier
     */
    public long getAsLong()
    {
        return this.get_();
    }

    @Override
    protected Long init_()
    {
        return supplier.getAsLong();
    }
}

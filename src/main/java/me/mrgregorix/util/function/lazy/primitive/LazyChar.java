package me.mrgregorix.util.function.lazy.primitive;

import me.mrgregorix.util.function.lazy.AbstractLazyPrimitive;
import me.mrgregorix.util.function.supplier.CharSupplier;

/**
 * Provided lazy initialization for primitive value {@code char}
 */
public class LazyChar extends AbstractLazyPrimitive<Character>
{
    private final CharSupplier supplier;

    public LazyChar(final CharSupplier supplier)
    {
        super(Character.class, char.class);
        this.supplier = supplier;
    }

    /**
     * Get the value from cache, if the value is not cached, it will
     * be got from the supplier
     */
    public char getAsChar()
    {
        return this.get_();
    }

    @Override
    protected Character init_()
    {
        return supplier.getAsChar();
    }
}

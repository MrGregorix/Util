package me.mrgregorix.util.function.lazy;

public abstract class AbstractLazyValue<T>
{
    protected           T           cached;
    protected           boolean     wasCached;

    /**
     * Construct new lazy value
     */
    protected AbstractLazyValue()
    {
    }

    /**
     * @return true if value was cached
     */
    public boolean isCached()
    {
        return wasCached;
    }

    /**
     * Remove value from cache and mark lazy value as uncached
     */
    public final void reset()
    {
        if (!this.wasCached || !this.reset_())
        {
            return;
        }

        this.wasCached = false;
        this.cached = null;
    }

    /**
     * Get the value from cache, if the value is not cached, it will
     * be got from the supplier
     */
    protected T get_()
    {
        if(this.wasCached)
        {
            return this.cached;
        }
        else
        {
            this.cached = this.init();
            this.wasCached = true;
            return this.cached;
        }
    }

    /**
     * Value will be got from
     */
    protected abstract T init();

    /**
     * Invoked when reset
     */
    protected abstract boolean reset_();
}

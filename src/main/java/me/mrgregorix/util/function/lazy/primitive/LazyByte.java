package me.mrgregorix.util.function.lazy.primitive;

import me.mrgregorix.util.function.lazy.AbstractLazyPrimitive;
import me.mrgregorix.util.function.supplier.ByteSupplier;

/**
 * Provided lazy initialization for primitive value {@code byte}
 */
public class LazyByte extends AbstractLazyPrimitive<Byte>
{
    private final ByteSupplier supplier;

    public LazyByte(final ByteSupplier supplier)
    {
        super(Byte.class, byte.class);
        this.supplier = supplier;
    }

    /**
     * Get the value from cache, if the value is not cached, it will
     * be got from the supplier
     */
    public byte getAsByte()
    {
        return this.get_();
    }

    @Override
    protected Byte init_()
    {
        return supplier.getAsByte();
    }
}

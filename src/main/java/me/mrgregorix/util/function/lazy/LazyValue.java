package me.mrgregorix.util.function.lazy;

import java.util.function.Supplier;

/**
 * Provided lazy initialization values in java
 */
public class LazyValue<T> extends AbstractLazyValue<T>
{
    private final Supplier<T> supplier;

    public LazyValue(final Supplier<T> supplier)
    {
        this.supplier = supplier;
    }

    /**
     * Get the value from cache, if the value is not cached, it will
     * be got from the supplier
     */
    public T get()
    {
        return super.get_();
    }

    @Override
    protected T init()
    {
        return supplier.get();
    }

    @Override
    protected boolean reset_()
    {
        return true;
    }
}

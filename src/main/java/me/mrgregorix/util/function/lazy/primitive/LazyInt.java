package me.mrgregorix.util.function.lazy.primitive;

import java.util.function.IntSupplier;

import me.mrgregorix.util.function.lazy.AbstractLazyPrimitive;
import me.mrgregorix.util.function.supplier.ShortSupplier;

/**
 * Provided lazy initialization for primitive value {@code int}
 */
public class LazyInt extends AbstractLazyPrimitive<Integer>
{
    private final IntSupplier supplier;

    public LazyInt(final IntSupplier supplier)
    {
        super(Integer.class, int.class);
        this.supplier = supplier;
    }

    /**
     * Get the value from cache, if the value is not cached, it will
     * be got from the supplier
     */
    public int getAsInt()
    {
        return this.get_();
    }

    @Override
    protected Integer init_()
    {
        return supplier.getAsInt();
    }
}

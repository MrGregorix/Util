package me.mrgregorix.util.function.lazy.primitive;

import me.mrgregorix.util.function.lazy.AbstractLazyPrimitive;
import me.mrgregorix.util.function.supplier.ShortSupplier;

/**
 * Provided lazy initialization for primitive value {@code short}
 */
public class LazyShort extends AbstractLazyPrimitive<Short>
{
    private final ShortSupplier supplier;

    public LazyShort(final ShortSupplier supplier)
    {
        super(Short.class, short.class);
        this.supplier = supplier;
    }

    /**
     * Get the value from cache, if the value is not cached, it will
     * be got from the supplier
     */
    public short getAsShort()
    {
        return this.get_();
    }

    @Override
    protected Short init_()
    {
        return supplier.getAsShort();
    }
}

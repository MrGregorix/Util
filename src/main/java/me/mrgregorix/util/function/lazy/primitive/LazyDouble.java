package me.mrgregorix.util.function.lazy.primitive;

import java.util.function.DoubleSupplier;

import me.mrgregorix.util.function.lazy.AbstractLazyPrimitive;

/**
 * Provided lazy initialization for primitive value {@code double}
 */
public class LazyDouble extends AbstractLazyPrimitive<Double>
{
    private final DoubleSupplier supplier;

    public LazyDouble(final DoubleSupplier supplier)
    {
        super(Double.class, double.class);
        this.supplier = supplier;
    }

    /**
     * Get the value from cache, if the value is not cached, it will
     * be got from the supplier
     */
    public double getAsDouble()
    {
        return this.get_();
    }

    @Override
    protected Double init_()
    {
        return supplier.getAsDouble();
    }
}

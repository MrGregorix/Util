package me.mrgregorix.util.function.lazy.primitive;

import me.mrgregorix.util.function.lazy.AbstractLazyPrimitive;
import me.mrgregorix.util.function.supplier.FloatSupplier;

/**
 * Provided lazy initialization for primitive value {@code float}
 */
public class LazyFloat extends AbstractLazyPrimitive<Float>
{
    private final FloatSupplier supplier;

    public LazyFloat(final FloatSupplier supplier)
    {
        super(Float.class, float.class);
        this.supplier = supplier;
    }

    /**
     * Get the value from cache, if the value is not cached, it will
     * be got from the supplier
     */
    public float getAsFloat()
    {
        return this.get_();
    }

    @Override
    protected Float init_()
    {
        return supplier.getAsFloat();
    }
}

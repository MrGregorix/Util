package me.mrgregorix.util.function.supplier;

import java.util.function.Supplier;

/**
 * Represents a supplier of {@code short}-valued results.  This is the
 * {@code short-producing primitive specialization of {@link
 * Supplier}.
 * <p>
 * <p>There is no requirement that a distinct result be returned each
 * time the supplier is invoked.
 * <p>
 * <p>This is a <a href="package-summary.html">functional
 * interface</a> whose functional method is {@link #getAsShort()}.
 *
 * @see Supplier
 * @since 1.8
 */
@FunctionalInterface
public interface ShortSupplier
{
    /**
     * Gets a result.
     *
     * @return a result
     */
    short getAsShort();
}

package me.mrgregorix.util.function.supplier;

import java.util.function.Supplier;

/**
 * Represents a supplier of {@code char}-valued results.  This is the
 * {@code char-producing primitive specialization of {@link
 * Supplier}.
 * <p>
 * <p>There is no requirement that a distinct result be returned each
 * time the supplier is invoked.
 * <p>
 * <p>This is a <a href="package-summary.html">functional
 * interface</a> whose functional method is {@link #getAsChar()}.
 *
 * @see Supplier
 * @since 1.8
 */
@FunctionalInterface
public interface CharSupplier
{
    /**
     * Gets a result.
     *
     * @return a result
     */
    char getAsChar();
}

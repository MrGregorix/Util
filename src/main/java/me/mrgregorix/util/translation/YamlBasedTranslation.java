package me.mrgregorix.util.translation;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;

import me.mrgregorix.util.bukkit.MBukkit;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class YamlBasedTranslation implements MessageSet
{
    private final YamlConfiguration configuration;

    public YamlBasedTranslation(final YamlConfiguration configuration)
    {
        MBukkit.bukkitMethod();
        this.configuration = configuration;
    }

    @Override
    public String get(final String s)
    {
        return this.configuration.getString(s);
    }

    public static MessageSet fromInputStream(final InputStream is)
    {
        return new YamlBasedTranslation(YamlConfiguration.loadConfiguration(new InputStreamReader(is)));
    }

    public static YamlBasedTranslation fromFile(final File file)
    {
        return new YamlBasedTranslation(YamlConfiguration.loadConfiguration(file));
    }

    public static YamlBasedTranslation fromJAR(final JavaPlugin plugin, final String path)
    {
        return new YamlBasedTranslation(YamlConfiguration.loadConfiguration(new InputStreamReader(plugin.getResource(path))));
    }
}

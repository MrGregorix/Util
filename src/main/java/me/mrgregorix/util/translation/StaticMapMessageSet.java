package me.mrgregorix.util.translation;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.Validate;

public class StaticMapMessageSet implements MessageSet
{
    private Map<String, String> translations;

    public StaticMapMessageSet(final String... translations)
    {
        Validate.isTrue(translations.length > 0 && translations.length %2 == 0, "Invalid translations");
        final Map<String, String> map  = new HashMap<>();

        for(int i = 0 ; i < translations.length ; i+= 2)
        {
            map.put(translations[i], translations[i + 1]);
        }

        this.translations = map;
    }

    public StaticMapMessageSet(final Map<String, String> map)
    {
        this.translations = map;
    }

    @Override
    public String get(final String translation)
    {
        return this.translations.get(translation);
    }
}

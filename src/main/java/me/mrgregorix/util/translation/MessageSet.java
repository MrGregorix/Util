package me.mrgregorix.util.translation;

import org.apache.commons.lang.StringUtils;

/**
 * Translation container
 */
public interface MessageSet
{
    String get(final String translation);

    default String get(final String translation, final String... args)
    {
        String out = this.get(translation);

        for(int i = 0 ; i < args.length ; i++)
        {
            out = StringUtils.replace(out, "{" + i + "}", args[i]);
        }

        return out;
    }
}

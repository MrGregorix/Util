package me.mrgregorix.util.translation;

public class WrappedTranslation implements MessageSet
{
    private MessageSet translation;
    private String prefix;

    public WrappedTranslation(final MessageSet translation, final String prefix)
    {
        this.translation = translation;
        this.prefix = prefix;
    }

    @Override
    public String get(final String s)
    {
        return translation.get(prefix + "." + s);
    }
}

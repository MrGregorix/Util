package me.mrgregorix.util.bukkit;

import me.mrgregorix.util.reflect.ReflectClass;

import org.bukkit.Bukkit;
import org.bukkit.command.SimpleCommandMap;

import org.apache.commons.lang.Validate;

/**
 * Simple bukkit util
 */
public final class MBukkit
{
    private static final boolean available = ReflectClass.forName("org.bukkit.Bukkit") != null;
    private static final SimpleCommandMap commandMap;

    static
    {
        if(!MBukkit.isAvailable())
        {
            commandMap = null;
        }
        else
        {
            commandMap = ReflectClass.forObject(Bukkit.getServer()).getDeclaredField("commandMap").access().as(SimpleCommandMap.class).get(Bukkit.getServer());
        }
    }

    private MBukkit() {}

    /**
     * Throws an exception if bukkit isn't available
     */
    public static void bukkitMethod()
    {
        Validate.isTrue(available, "there's not bukkit");
    }

    /**
     * @return true if there's bukkit
     */
    public static boolean isAvailable()
    {
        return available;
    }

    /**
     * @return bukkit command map
     */
    public static SimpleCommandMap getCommandMap()
    {
        MBukkit.bukkitMethod();
        return commandMap;
    }
}

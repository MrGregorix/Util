package me.mrgregorix.util.bukkit;

import me.mrgregorix.util.reflect.ReflectClass;

import org.bukkit.Bukkit;

public final class BukkitReflect
{
    private static final String version;
    private static final String nms;
    private static final String obc;

    static
    {
        if (!MBukkit.isAvailable())
        {
            version = nms = obc = null;
        }
        else
        {
            final String packageName = Bukkit.getServer().getClass().getPackage().getName();
            version = packageName.substring(packageName.lastIndexOf(".") + 1);

            nms = "net.minecraft.server." + version;
            obc = "org.bukkit.craftbukkit." + version;
        }
    }

    private BukkitReflect()
    {
    }

    /**
     * @return full net.minecraft.server package name
     */
    public static String getNmsPackage()
    {
        MBukkit.bukkitMethod();
        return nms;
    }

    /**
     * @return full org.bukkit.craftbukkit package name
     */
    public static String getObcPackage()
    {
        MBukkit.bukkitMethod();
        return obc;
    }


    /**
     * Get the class from nms
     *
     * @param name class name
     *
     * @return nms class
     */
    public static ReflectClass<?> getNmsClass(final String name)
    {
        MBukkit.bukkitMethod();
        return ReflectClass.forName(nms + "." + name);
    }


    /**
     * Get the class from obc
     *
     * @param name class name
     *
     * @return obc class
     */
    public static ReflectClass<?> getObcClass(final String name)
    {
        MBukkit.bukkitMethod();
        return ReflectClass.forName(obc + "." + name);
    }
}

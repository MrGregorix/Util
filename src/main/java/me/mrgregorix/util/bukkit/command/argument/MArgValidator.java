package me.mrgregorix.util.bukkit.command.argument;

import me.mrgregorix.util.bukkit.command.MCommand;

public abstract class MArgValidator<T>
{
    protected MArgValidator()
    {
    }

    protected void invalid(final MCommand mCommand, final int index)
    {
        this.invalid(mCommand.getTranslations().get("invalidArg", String.valueOf(index)));
    }

    protected void invalid(final String message)
    {
        throw new CommandException(message);
    }

    public abstract T validate(MCommand mCommand, int index, String[] arguments);
}

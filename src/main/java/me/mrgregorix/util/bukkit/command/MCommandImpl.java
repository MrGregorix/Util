package me.mrgregorix.util.bukkit.command;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import me.mrgregorix.util.bukkit.command.annotation.MCommandAlias;
import me.mrgregorix.util.bukkit.command.annotation.MCommandAliases;
import me.mrgregorix.util.bukkit.command.annotation.MCommandMethod;
import me.mrgregorix.util.bukkit.command.annotation.MRequiredArgsRange;
import me.mrgregorix.util.bukkit.command.annotation.MRequiredPermissions;
import me.mrgregorix.util.bukkit.command.annotation.MRequiredSender;
import me.mrgregorix.util.bukkit.command.argument.CommandException;
import me.mrgregorix.util.reflect.ReflectMethod;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;

class MCommandImpl extends Command
{
    protected final MCommand         mCommand;
    protected final ReflectMethod    executor;
    protected final List<Annotation> annotations;
    protected       MCommandListener listener;

    protected String description;
    protected String usage;
    protected int minArgs = 0;
    protected int maxArgs = -1;
    protected String                         permissions;
    protected Class<? extends CommandSender> sender;

    @MRequiredPermissions("a")
    protected static String genName(final List<Annotation> annotations)
    {
        for (final Annotation annotation : annotations)
        {
            if (annotation instanceof MCommandMethod)
            {
                return ((MCommandMethod)annotation).name();
            }
        }
        throw new IllegalArgumentException("Can't find command annotation");
    }

    MCommandImpl(final MCommand mCommand, final MCommandListener listener, final ReflectMethod executor, final List<Annotation> annotations)
    {
        this(genName(annotations), mCommand, listener, executor, annotations);
    }

    MCommandImpl(final String name, final MCommand mCommand, final MCommandListener listener, final ReflectMethod executor, final List<Annotation> annotations)
    {
        super(name);
        this.mCommand = mCommand;
        this.listener = listener;
        this.executor = executor;
        this.annotations = annotations;

        final List<String> aliases = new ArrayList<>();

        for (final Annotation annotation : annotations)
        {
            if (annotation instanceof MCommandMethod)
            {
                MCommandMethod mCommandMethod = (MCommandMethod) annotation;
                this.setDescription(description = mCommandMethod.description());
                this.setUsage(usage = (mCommandMethod.usage().isEmpty() ? mCommand.getTranslations().get("invalidUsage") : mCommandMethod.usage()));
            }
            else if (annotation instanceof MRequiredArgsRange)
            {
                MRequiredArgsRange mRequiredArgsRange = (MRequiredArgsRange) annotation;
                Validate.isTrue(mRequiredArgsRange.min() > 0 && (mRequiredArgsRange.max() == -1 || mRequiredArgsRange.max() >= mRequiredArgsRange.min()), "Invalid args range");
                this.minArgs = mRequiredArgsRange.min();
                this.maxArgs = mRequiredArgsRange.max();
            }
            else if (annotation instanceof MRequiredSender)
            {
                this.sender = ((MRequiredSender) annotation).value();
            }
            else if (annotation instanceof MRequiredPermissions)
            {
                this.setPermission(permissions = ((MRequiredPermissions) annotation).value());
            }
            else if (annotation instanceof MCommandAliases)
            {
                for(final MCommandAlias alias : ((MCommandAliases)annotation).value())
                {
                    aliases.add(alias.value().toLowerCase());
                }
            }
        }

        this.setAliases(aliases);
    }

    public Class<? extends CommandSender> getSender()
    {
        return sender;
    }

    public int getMaxArgs()
    {
        return maxArgs;
    }

    public int getMinArgs()
    {
        return minArgs;
    }

    public List<Annotation> getAnnotations()
    {
        return annotations;
    }

    @Override
    public boolean execute(final CommandSender commandSender, final String s, final String[] strings)
    {
        if (this.sender != null && !this.sender.isInstance(commandSender))
        {
            commandSender.sendMessage(ChatColor.RED + ChatColor.translateAlternateColorCodes('&', mCommand.getTranslations().get("invalidUsage_sender")));
            return true;
        }

        if (this.permissions != null && !commandSender.hasPermission(this.permissions))
        {
            commandSender.sendMessage(ChatColor.RED + ChatColor.translateAlternateColorCodes('&', mCommand.getTranslations().get("noPermissions", this.permissions)));
            return true;
        }

        if (strings.length < this.minArgs)
        {
            commandSender.sendMessage(ChatColor.RED + ChatColor.translateAlternateColorCodes('&', mCommand.getTranslations().get("invalidUsage_few", StringUtils.replace(this.usage, "<cmd>", s))));
            return true;
        }

        if (this.maxArgs != -1 && strings.length > this.maxArgs)
        {
            commandSender.sendMessage(ChatColor.RED + ChatColor.translateAlternateColorCodes('&', mCommand.getTranslations().get("invalidUsage_much", StringUtils.replace(this.usage, "<cmd>", s))));
            return true;
        }

        try
        {
            executor.invoke(this.listener, commandSender, s, strings);
        }
        catch (final RuntimeException ex)
        {
            if (ex.getCause() instanceof CommandException)
            {
                commandSender.sendMessage(ChatColor.RED + ChatColor.translateAlternateColorCodes('&', ex.getCause().getMessage()));
            }
            else
            {
                throw ex;
            }
        }

        return true;
    }
}

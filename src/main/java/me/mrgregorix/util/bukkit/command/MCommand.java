package me.mrgregorix.util.bukkit.command;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import me.mrgregorix.util.bukkit.MBukkit;
import me.mrgregorix.util.bukkit.command.annotation.MCommandMethod;
import me.mrgregorix.util.reflect.ReflectClass;
import me.mrgregorix.util.reflect.ReflectMethod;
import me.mrgregorix.util.translation.MessageSet;

import org.bukkit.command.Command;
import org.bukkit.plugin.java.JavaPlugin;

import org.apache.commons.lang.Validate;

/**
 * Base command util class
 */
public final class MCommand
{
    private final JavaPlugin plugin;
    private final MessageSet translations;
    private final Set<Command> mCommandList = new HashSet<>();

    public MCommand(final JavaPlugin plugin)
    {
        this(plugin, new MCommandDefaultMessageSet());
    }

    public MCommand(final JavaPlugin plugin, final MessageSet translations)
    {
        MBukkit.bukkitMethod();
        Validate.notNull(plugin, "plugin");
        this.plugin = plugin;
        this.translations = translations;
    }

    public JavaPlugin getPlugin()
    {
        return plugin;
    }

    public MessageSet getTranslations()
    {
        return translations;
    }

    public List<Command> registerListener(final MCommandListener listener)
    {
        final List<Command> commands = ReflectClass.forObject(listener).getDeclaredMethods().stream()
                                                   .filter(method -> method.getHandle().isAnnotationPresent(MCommandMethod.class))
                                                   .map(method -> new MCommandImpl(this, listener, method.access(), Arrays.asList(method.getHandle().getDeclaredAnnotations())))
                                                   .collect(Collectors.toList());

        MBukkit.getCommandMap().registerAll(this.plugin.getName().toLowerCase(), commands);

        return commands;
    }

    public Set<Command> getCommandList()
    {
        return mCommandList;
    }
}

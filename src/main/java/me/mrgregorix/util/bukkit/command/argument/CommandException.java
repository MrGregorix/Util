package me.mrgregorix.util.bukkit.command.argument;

/**
 * For internal usage
 */
public class CommandException extends RuntimeException
{
    public CommandException(final String message)
    {
        super(message);
    }

    /**
     * We don't need a stacktrace
     */
    @Override
    public Throwable fillInStackTrace()
    {
        return this;
    }
}

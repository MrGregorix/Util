package me.mrgregorix.util.bukkit.command.argument.validators;

import me.mrgregorix.util.bukkit.command.argument.MArg;

import org.bukkit.entity.Player;

public final class DefaultValidators
{
    private DefaultValidators()
    {
    }

    public static void register()
    {
        MArg.registerValidator(Integer.class, new IntValidator());
        MArg.registerValidator(Player.class, new PlayerValidator());
    }
}

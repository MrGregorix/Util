package me.mrgregorix.util.bukkit.command.argument.validators;

import me.mrgregorix.util.bukkit.command.MCommand;
import me.mrgregorix.util.bukkit.command.argument.MArgValidator;
import me.mrgregorix.util.math.Parse;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

final class PlayerValidator extends MArgValidator<Player>
{
    @Override
    public Player validate(final MCommand mCommand, final int index, final String[] arguments)
    {
        final Player player = Bukkit.getServer().getPlayer(arguments[index]);

        if(player == null)
        {
            invalid(mCommand.getTranslations().get("arg_player", arguments[index]));
        }

        return player;
    }
}

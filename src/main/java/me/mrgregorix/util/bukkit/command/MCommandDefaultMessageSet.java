package me.mrgregorix.util.bukkit.command;

import me.mrgregorix.util.translation.StaticMapMessageSet;

final class MCommandDefaultMessageSet extends StaticMapMessageSet
{
    MCommandDefaultMessageSet()
    {
        super(
                "invalidUsage", "Invalid usage! &2Use: &a{0}",
                "invalidUsage_few", "Invalid usage! &eToo few arguments! &2Use: &a{0}",
                "invalidUsage_much", "Invalid usage! &eToo much arguments! &2Use &a{0}",
                "invalidUsage_sender", "You can't use this command",
                "noPermissions", "You don't have permissions to do this ({0})",
                "arg", "Argument &e{0} &cmust be &e{1}",
                "arg_player", "Can't find player &e{0}",
                "arg_name_int", "an integer"
        );
    }
}

package me.mrgregorix.util.bukkit.command.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.bukkit.command.CommandSender;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface MRequiredSender
{
    Class<? extends CommandSender> value();
}

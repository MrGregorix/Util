package me.mrgregorix.util.bukkit.command.argument;

import java.util.HashMap;
import java.util.Map;

import me.mrgregorix.util.bukkit.command.MCommand;
import me.mrgregorix.util.bukkit.command.argument.validators.DefaultValidators;

public final class MArg
{
    private static final Map<Class<?>, MArgValidator<?>> validators = new HashMap<>();

    public static <T> void registerValidator(final Class<T> clazz, final MArgValidator<T> validator)
    {
        validators.put(clazz, validator);
    }

    static
    {
        DefaultValidators.register();
    }

    public static <T> T require(final MCommand mCommand, final String[] args, final int index, final Class<T> clazz)
    {
        //noinspection unchecked
        final MArgValidator<T> validator = (MArgValidator<T>)validators.get(clazz);
        if(validator == null)
        {
            throw new CommandException("Couldn't find validator for " + clazz.getName());
        }
        if(index >= args.length)
        {
            throw new CommandException(mCommand.getTranslations().get("invalidUsage_few"));
        }

        return validator.validate(mCommand, index, args);
    }
}

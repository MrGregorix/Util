package me.mrgregorix.util.bukkit.command.argument.validators;

import me.mrgregorix.util.bukkit.command.MCommand;
import me.mrgregorix.util.bukkit.command.argument.MArgValidator;
import me.mrgregorix.util.math.Parse;

final class IntValidator extends MArgValidator<Integer>
{
    @Override
    public Integer validate(final MCommand mCommand, final int index, final String[] arguments)
    {
        final Integer integer = Parse.parseInt(arguments[index]);

        if(integer == null)
        {
            invalid(mCommand.getTranslations().get("arg", String.valueOf(index), mCommand.getTranslations().get("arg_name_int")));
        }

        return integer;
    }
}

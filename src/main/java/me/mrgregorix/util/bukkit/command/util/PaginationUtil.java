package me.mrgregorix.util.bukkit.command.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class PaginationUtil
{
    private PaginationUtil()
    {
    }

    public static List<String> paginate(final List<String> list, final int page, final int resultsPerPage)
    {
        if(list.size() <= resultsPerPage)
        {
            return list;
        }

        final List<String> output = new ArrayList<>(resultsPerPage);

        for(int i = 0 ; i < resultsPerPage ; i++)
        {
            int index = Math.max(0, page - 1) * resultsPerPage + i;
            if(index >= list.size())
            {
                break;
            }

            output.add(list.get(index));
        }

        return output;
    }
}

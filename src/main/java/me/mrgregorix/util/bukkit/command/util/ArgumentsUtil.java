package me.mrgregorix.util.bukkit.command.util;

public final class ArgumentsUtil
{
    private ArgumentsUtil()
    {
    }

    public static String[] cutArgs(final String[] args, final int index)
    {
        final String[] newArgs = new String[args.length - index];
        System.arraycopy(args, index, newArgs, 0, newArgs.length);
        return newArgs;
    }
}

package me.mrgregorix.util.reflect;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Wrapper for java constructor
 */
public final class ReflectConstructor<T> implements ReflectAccessibleObject<Constructor<T>, ReflectConstructor>
{
    private Constructor<T> handle;

    ReflectConstructor(Constructor<T> handle)
    {
        this.handle = handle;
    }

    public Constructor<T> getHandle()
    {
        return this.handle;
    }

    /**
     * Invokes the constructor given parameters as arguments
     *
     * @param parameters arguments
     *
     * @return instance result
     */
    public T newInstance(final Object... parameters)
    {
        try
        {
            return this.handle.newInstance(parameters);
        }
        catch (IllegalAccessException | InstantiationException ex)
        {
            throw new IllegalStateException(ex);
        }
        catch (InvocationTargetException ex)
        {
            throw new RuntimeException("Invocation error", ex.getTargetException());
        }
    }

    /**
     * Checks if constructor has modifier
     *
     * @param modifier modifier to test
     *
     * @return true if has, false if not
     */
    public boolean hasModifier(final int modifier)
    {
        return (this.handle.getModifiers() & modifier) != 0;
    }
}

package me.mrgregorix.util.reflect;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

/**
 * Internal utilities for all reflections
 */
@SuppressWarnings("unchecked")
public final class InternalReflect
{
    private InternalReflect()
    {
    }

    /**
     * Fast low level exception checker
     */
    @FunctionalInterface
    public interface SneakyThrow
    {
        void run() throws Exception;
    }

    /**
     * Search values specified in iterator convert it and return it as
     * new List
     *
     * @param array     an array of values
     * @param predicate Logical test
     * @param convert   Converter
     * @param <S>       Given value
     * @param <E>       Expected return
     *
     * @return list of results
     */
    public static <S, E> List<E> search(final S[] array, final Predicate<S> predicate, final Function<S, E> convert)
    {
        return search((Iterable) Arrays.asList(array), predicate, convert);
    }

    /**
     * Search values specified in iterator convert it and return it as
     * new List
     *
     * @param iterable  iterable element that stores the value
     * @param predicate Logical test
     * @param convert   Converter
     * @param <S>       Given value
     * @param <E>       Expected return
     *
     * @return list of results
     */
    public static <S, E> List<E> search(final Iterable<S> iterable, final Predicate<S> predicate, final Function<S, E> convert)
    {
        return search(iterable.iterator(), predicate, convert);
    }

    /**
     * Search values specified in iterator convert it and return it as
     * new List
     *
     * @param iterator  Iterator that stores the values
     * @param predicate Logical test
     * @param convert   Converter
     * @param <S>       Given value
     * @param <E>       Expected return
     *
     * @return list of results
     */
    public static <S, E> List<E> search(final Iterator<S> iterator, final Predicate<S> predicate, final Function<S, E> convert)
    {
        final List<E> list = Lists.newArrayList();

        while (iterator.hasNext())
        {
            final S next = iterator.next();
            if (predicate.test(next))
            {
                list.add(convert.apply(next));
            }
        }

        return list;
    }

    /**
     * Creates an immutable list of mapped values of given array
     *
     * @param array  initial value
     * @param mapper value mapper
     * @param <A>    input type
     * @param <R>    output type
     *
     * @return immutable list
     */
    public static <A, R> List<R> buildList(final A[] array, final Function<A, R> mapper)
    {
        return ImmutableList.copyOf(Stream.of(array).map(mapper).collect(Collectors.toList()));
    }

    /**
     * Accepts list of two types of arguments, and returning list of
     * first type, values from first list of second type will be
     * mapped and stored in output list too
     *
     * @param unknownList list of two types
     * @param t           first type class
     * @param u           second type class
     * @param convert     mapper
     * @param <T>         first type, expected output
     * @param <U>         second type, will be mapped
     *
     * @return list of first type
     */
    public static <T, U> List<T> connect(final List<?> unknownList, final Class<T> t, final Class<U> u, final Function<U, T> convert)
    {
        return Arrays.asList(connect(unknownList.toArray(), t, u, convert));
    }

    /**
     * Accepts array of two types of arguments, and returning array of
     * first type, values from first array of second type will be
     * mapped and stored in output array too
     *
     * @param unknownArray array of two types
     * @param t            first type class
     * @param u            second type class
     * @param convert      mapper
     * @param <T>          first type, expected output
     * @param <U>          second type, will be mapped
     *
     * @return array of first type
     */
    public static <T, U> T[] connect(final Object[] unknownArray, final Class<T> t, final Class<U> u, final Function<U, T> convert)
    {
        final T[] array = (T[]) Array.newInstance(t, unknownArray.length);

        for (int i = 0; i < array.length; ++i)
        {
            final Object element = unknownArray[i];
            if (t.isInstance(element))
            {
                array[i] = (T) element;
            }
            else if (u.isInstance(element))
            {
                array[i] = convert.apply((U) unknownArray[i]);
            }
            else
            {
                array[i] = null;
            }
        }

        return array;
    }

    /**
     * Run given method, when exception has been thrown prints
     * stacktrace on stdout
     *
     * @param exception see {@link SneakyThrow}
     */
    public static void sneakyThrow(final InternalReflect.SneakyThrow exception)
    {
        try
        {
            exception.run();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
}

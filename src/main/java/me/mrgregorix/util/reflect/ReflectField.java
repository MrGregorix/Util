package me.mrgregorix.util.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import org.apache.commons.lang.Validate;

/**
 * Wrapper for java field
 *
 * @param <T> field type
 */
@SuppressWarnings("unchecked")
public final class ReflectField<T> implements ReflectAccessibleObject<Field, ReflectField<T>>
{
    private final Field handle;

    ReflectField(final Field handle)
    {
        this.handle = handle;
    }

    public Field getHandle()
    {
        return this.handle;
    }

    /**
     * Converts field to other type field
     *
     * @param as  target type class
     * @param <R> target type
     *
     * @return converted field, or null if given type is invalid
     */
    public <R> ReflectField<R> as(final Class<R> as)
    {
        return as == this.getHandle().getType() ? (ReflectField<R>) this : null;
    }

    /**
     * Get field value
     *
     * @param target object that a value will be got from
     *
     * @return got value
     */
    public T get(final Object target)
    {
        Validate.isTrue(target != null || isStatic(), "not static field");

        try
        {
            return (T) this.handle.get(target);
        }
        catch (final IllegalAccessException ex)
        {
            throw new IllegalStateException(ex);
        }
    }

    /**
     * @return true if field is static, false if it isn't
     */
    public boolean isStatic()
    {
        return this.hasModifier(Modifier.STATIC);
    }

    /**
     * Checks if field has modifier
     *
     * @param modifier modifier to test
     *
     * @return true if has, false if not
     */
    public boolean hasModifier(final int modifier)
    {
        return (this.handle.getModifiers() & modifier) != 0;
    }

    /**
     * Get field static field value
     *
     * @return got value
     */
    public T getStatic()
    {
        return this.get(null);
    }

    /**
     * Set static field value
     *
     * @param value value to set
     */
    public void setStatic(final T value)
    {
        this.set(null, value);
    }

    /**
     * Set field value
     *
     * @param target object that a value will be got from
     * @param value  value to set
     */
    public void set(final Object target, final T value)
    {
        Validate.isTrue(target != null || isStatic(), "not static field");

        try
        {
            this.handle.set(target, value);
        }
        catch (IllegalAccessException var4)
        {
            throw new IllegalStateException(var4);
        }
    }
}

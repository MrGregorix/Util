package me.mrgregorix.util.reflect;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.apache.commons.lang.Validate;

/**
 * Wrapper for java method
 */
public final class ReflectMethod implements ReflectAccessibleObject<Method, ReflectMethod>
{
    private Method handle;

    ReflectMethod(Method handle)
    {
        this.handle = handle;
    }

    public Method getHandle()
    {
        return this.handle;
    }

    /**
     * Invokes the method on given target using given parameters as
     * method arguments
     *
     * @param target     target
     * @param parameters arguments
     *
     * @return method result
     */
    public Object invoke(final Object target, final Object... parameters)
    {
        Validate.isTrue(target != null || isStatic(), "not static method");

        try
        {
            return this.handle.invoke(target, parameters);
        }
        catch (IllegalAccessException ex)
        {
            throw new IllegalStateException(ex);
        }
        catch (InvocationTargetException ex)
        {
            throw new RuntimeException("Invocation error", ex.getTargetException());
        }
    }

    /**
     * @return true if method is static, false if it isn't
     */
    public boolean isStatic()
    {
        return this.hasModifier(Modifier.STATIC);
    }

    /**
     * Checks if method has modifier
     *
     * @param modifier modifier to test
     *
     * @return true if has, false if not
     */
    public boolean hasModifier(final int modifier)
    {
        return (this.handle.getModifiers() & modifier) != 0;
    }

    /**
     * Invokes the static method using given parameters as method
     * arguments
     *
     * @param parameters arguments
     *
     * @return method result
     */
    public Object invokeStatic(final Object... parameters)
    {
        return this.invoke(null, parameters);
    }
}

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package me.mrgregorix.util.reflect;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import me.mrgregorix.util.function.cache.Cache;
import me.mrgregorix.util.function.cache.Caches;

import org.apache.commons.lang.Validate;

@SuppressWarnings("unchecked")
public final class ReflectClass<T> implements ReflectObject<Class<T>>
{
    /**
     * all cached class
     */
    private static final Cache<String, ReflectClass> cache = Caches.newHashMapBasedCache();

    private final Class<T> handle;

    /* list of methods and fields, for fastest getting */
    private final List<ReflectMethod>         methods;
    private final List<ReflectMethod>         declaredMethods;
    private final List<ReflectField<?>>       fields;
    private final List<ReflectField<?>>       declaredFields;
    private final List<ReflectConstructor<T>> constuctors;
    private final List<ReflectConstructor<T>> declaredConstructors;

    private ReflectClass(final Class<T> handle)
    {
        this.handle = handle;

        // convert all fields and methods and save it
        this.methods = InternalReflect.buildList(handle.getMethods(), ReflectMethod::new);
        this.declaredMethods = InternalReflect.buildList(handle.getDeclaredMethods(), ReflectMethod::new);
        this.fields = InternalReflect.buildList(handle.getFields(), ReflectField::new);
        this.declaredFields = InternalReflect.buildList(handle.getDeclaredFields(), ReflectField::new);
        this.constuctors = InternalReflect.buildList(handle.getConstructors(), (h) -> new ReflectConstructor<>((Constructor<T>) h));
        this.declaredConstructors = InternalReflect.buildList(handle.getDeclaredConstructors(), (h) -> new ReflectConstructor<>((Constructor<T>) h));
    }

    public Class<T> getHandle()
    {
        return this.handle;
    }

    /**
     * Simply convert class
     *
     * @param as  expected output class
     * @param <E> expected output
     *
     * @return converted class
     */
    public <E> ReflectClass<E> as(final Class<E> as)
    {
        return (ReflectClass<E>) this;
    }

    /**
     * @return methods in the class
     */
    public List<ReflectMethod> getMethods()
    {
        return this.methods;
    }

    /**
     * @return declared methods in the class
     */
    public List<ReflectMethod> getDeclaredMethods()
    {
        return this.declaredMethods;
    }

    /**
     * @return fields in the class
     */
    public List<ReflectField<?>> getFields()
    {
        return this.fields;
    }

    /**
     * @return declared fields in the class
     */
    public List<ReflectField<?>> getDeclaredFields()
    {
        return this.declaredFields;
    }

    /**
     * Get the declared method
     *
     * @param name           method name
     * @param parameterTypes types of parameters
     *
     * @return method
     */
    public ReflectMethod getDeclaredMethod(final String name, final Object... parameterTypes)
    {
        return this.getMethod0(name, this.declaredMethods, parameterTypes);
    }

    /**
     * Get the method
     *
     * @param name           method name
     * @param parameterTypes types of parameters
     *
     * @return method
     */
    public ReflectMethod getMethod(final String name, final Object... parameterTypes)
    {
        return this.getMethod0(name, this.methods, parameterTypes);
    }

    /**
     * helper for getting method
     */
    private ReflectMethod getMethod0(final String name, final List<ReflectMethod> methodPool, final Object... parameterTypes)
    {
        Validate.notNull(name, "name");
        Class[] cTypes = InternalReflect.connect(parameterTypes, Class.class, ReflectClass.class, ReflectClass::getHandle);
        return this.get0(methodPool, (m) -> m.getName().equals(name) && Arrays.equals(m.getParameterTypes(), cTypes));
    }

    /**
     * Get the declared field
     *
     * @param name field name
     *
     * @return field
     */
    public ReflectField<?> getDeclaredField(final String name)
    {
        return this.getField0(name, this.declaredFields);
    }

    /**
     * Get the field
     *
     * @param name field name
     *
     * @return field
     */
    public ReflectField<?> getField(final String name)
    {
        return this.getField0(name, this.fields);
    }


    /**
     * Get the declared field by type
     *
     * @param type type
     *
     * @return field
     */
    public <R> ReflectField<R> getDeclaredFieldByReturnType(final Class<R> type)
    {
        ReflectField<R>[] fields = this.getDeclaredFieldsByReturnType(type);
        return fields.length != 1 ? null : fields[0];
    }

    /**
     * Get the declared field by type
     *
     * @param type type
     *
     * @return field
     */
    public <R> ReflectField<R> getDeclaredFieldByReturnType(final ReflectClass<R> type)
    {
        return this.getDeclaredFieldByReturnType(type.getHandle());
    }

    /**
     * Get the field by type
     *
     * @param type type
     *
     * @return field
     */
    public <R> ReflectField<R> getFieldByReturnType(final Class<R> type)
    {
        ReflectField<R>[] fields = this.getFieldsByReturnType(type);
        return fields.length != 1 ? null : fields[0];
    }

    /**
     * Get the field by type
     *
     * @param type type
     *
     * @return field
     */
    public <R> ReflectField<R> getFieldByReturnType(final ReflectClass<R> type)
    {
        return this.getFieldByReturnType(type.getHandle());
    }


    /**
     * Get the fields by type
     *
     * @param type type
     *
     * @return field
     */
    public <R> ReflectField<R>[] getDeclaredFieldsByReturnType(final Class<R> type)
    {
        return this.getFieldsByReturnType0(type, this.declaredFields);
    }

    /**
     * Get the fields by type
     *
     * @param type type
     *
     * @return field
     */
    public <R> ReflectField<R>[] getDeclaredFieldsByReturnType(final ReflectClass<R> type)
    {
        return this.getDeclaredFieldsByReturnType(type.getHandle());
    }

    /**
     * Get the fields by type
     *
     * @param type type
     *
     * @return field
     */
    public <R> ReflectField<R>[] getFieldsByReturnType(final Class<R> type)
    {
        return this.getFieldsByReturnType0(type, this.fields);
    }

    /**
     * Get the fields by type
     *
     * @param type type
     *
     * @return field
     */
    public <R> ReflectField<R>[] getFieldsByReturnType(ReflectClass<R> type)
    {
        return this.getFieldsByReturnType(type.getHandle());
    }

    /**
     * helper for getting fields
     */
    private ReflectField[] getFieldsByReturnType0(final Object returnType, final List<ReflectField<?>> fieldPool)
    {
        Validate.notNull(returnType, "returnType");

        final Class unifiedType = (InternalReflect.connect(new Object[]{returnType}, Class.class, ReflectClass.class, ReflectClass::getHandle))[0];
        final List list = (List) fieldPool.stream().filter((type) -> type.getHandle().getType() == unifiedType).collect(Collectors.toList());
        return (ReflectField[]) list.toArray(new ReflectField[list.size()]);
    }

    /**
     * helper for getting fields
     */
    private ReflectField getField0(final String name, final List<ReflectField<?>> fieldPool)
    {
        Validate.notNull(name, "name");
        return this.get0(fieldPool, (field) -> field.getName().equals(name));
    }

    public List<ReflectConstructor<T>> getDeclaredConstructors()
    {
        return declaredConstructors;
    }

    public List<ReflectConstructor<T>> getConstuctors()
    {
        return constuctors;
    }

    public ReflectConstructor<T> getDeclaredConstructor(final Class<?>... parameterTypes)
    {
        return this.getConstructor0(this.declaredConstructors, parameterTypes);
    }

    public ReflectConstructor<T> getDeclaredConstructor(final ReflectClass<?>... parameterTypes)
    {
        return this.getConstructor0(this.declaredConstructors, parameterTypes);
    }

    public ReflectConstructor<T> getConstructor(final Class<?>... parameterTypes)
    {
        return this.getConstructor0(this.constuctors, parameterTypes);
    }

    public ReflectConstructor<T> getConstructor(final ReflectClass<?>... parameterTypes)
    {
        return this.getConstructor0(this.constuctors, parameterTypes);
    }


    private ReflectConstructor<T> getConstructor0(final List<ReflectConstructor<T>> constructorPool, final Object... parameterTypes)
    {
        final Class<?>[] parameterTypesClass = InternalReflect.connect(parameterTypes, Class.class, ReflectClass.class, ReflectClass::getHandle);
        return get0(constructorPool, (ctor) -> Arrays.equals(ctor.getParameterTypes(), parameterTypesClass));
    }

    /**
     * helper method for all getters
     */
    private <R, I extends ReflectObject<R>> I get0(List<I> pool, Predicate<R> predicate)
    {
        final List<I> list = pool.stream().filter((s) -> predicate.test(s.getHandle())).collect(Collectors.toList());
        return list.size() <= 0 ? null : list.get(0);
    }

    @Override
    public String toString()
    {
        return Modifier.toString(this.handle.getModifiers()) + " " + this.handle.toString();
    }

    /**
     * Return class for given full class name
     *
     * @param name class name
     *
     * @return class
     */
    public static ReflectClass<?> forName(final String name)
    {
        Validate.notNull(name, "name");
        return (ReflectClass) cache.getCached(name, (supplier) -> {
            try
            {
                return forClass(Class.forName(name));
            }
            catch (ClassNotFoundException var3)
            {
                return null;
            }
        });
    }

    /**
     * Return class for java class
     *
     * @param clazz class
     *
     * @return class
     */
    public static <T> ReflectClass<T> forClass(final Class<T> clazz)
    {
        Validate.notNull(clazz, "clazz");
        return (ReflectClass<T>) cache.getCached(clazz.getName(), (name) -> new ReflectClass(clazz));
    }

    /**
     * Return class for obkect
     *
     * @param object object
     *
     * @return class
     */
    public static <T> ReflectClass<T> forObject(final T object)
    {
        Validate.notNull(object, "object");
        return forClass((Class<T>) object.getClass());
    }
}

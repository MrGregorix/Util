package me.mrgregorix.util.reflect;

import java.lang.reflect.AccessibleObject;

/**
 * An interface that is extending by all wrappers that wraps the
 * {@link AccessibleObject} class
 *
 * @param <H> handle type
 * @param <M> parent class type
 */
public interface ReflectAccessibleObject<H extends AccessibleObject, M extends ReflectAccessibleObject> extends ReflectObject<H>
{
    /**
     * @return true if object is accessible from everywhere, false if
     * not
     */
    default boolean isAccessible()
    {
        return this.getHandle().isAccessible();
    }

    /**
     * Makes object accessible or not accessible
     *
     * @param accessible accessible state
     *
     * @return itself
     */
    default M setAccessible(final boolean accessible)
    {
        if (!this.isAccessible())
        {
            this.getHandle().setAccessible(accessible);
        }

        //noinspection unchecked
        return (M) this;
    }

    /**
     * Same as {@code setAccessible(true)}
     *
     * @return itself
     */
    default M access()
    {
        return this.setAccessible(true);
    }
}

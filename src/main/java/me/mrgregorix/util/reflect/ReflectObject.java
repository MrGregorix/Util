package me.mrgregorix.util.reflect;

/**
 * All reflect util objects wrapped the java reflection object will be
 * extend this class
 *
 * @param <T> java reflection type
 */
public interface ReflectObject<T>
{

    /**
     * @return the handle of java reflection api
     */
    T getHandle();
}

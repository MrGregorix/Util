package me.mrgregorix.util;

import org.apache.commons.lang.Validate;

import java.util.function.Predicate;

public final class Equality
{
    private Equality()
    {
    }

    /**
     * Calculates hash code of the object
     *
     * @param objects class members
     *
     * @return hash code
     */
    public static int hashCode(final Object... objects)
    {
        Validate.noNullElements(objects, "objects");

        int result = 1;

        for (final Object object : objects)
        {
            result = 31 * result * (object != null ? object.hashCode() : 0);
        }

        return result;
    }

    /**
     * @return true if elements are same or elements are
     * equal and having same hash code
     */
    public static <T> boolean hashEquals(final T obj1, final T obj2)
    {
        return obj1 == obj2 || (obj1 != null && obj2 != null && (obj1.equals(obj2) && obj1.hashCode() == obj2.hashCode()));
    }

    /**
     * @return true if elements are same or their types are
     * same
     */
    public static <T> boolean shallowEquals(final T obj1, final T obj2)
    {
        return obj1 == obj2 || (obj1 != null && obj2 != null && (obj1.getClass() == obj2.getClass()));
    }

    /**
     * @return true if pairs of elements are #hashEquals
     */
    public static boolean deepEquals(final Object... objectPairs)
    {
        Validate.notNull(objectPairs, "objectPairs");
        Validate.isTrue(objectPairs.length % 2 == 0, "invalid pairs");

        for(int i = 0 ; i < objectPairs.length / 2 ; i++)
        {
            if(!hashEquals(objectPairs[i * 2], objectPairs[i * 2 + 1]))
            {
                return false;
            }
        }
        return true;
    }

    public static <T> boolean advancedEquals(final T obj1, final Object obj2, Predicate<T> comparator)
    {
        if(!shallowEquals(obj1, obj2))
        {
            return false;
        }
        //noinspection unchecked
        return comparator.test((T)obj2);
    }
}

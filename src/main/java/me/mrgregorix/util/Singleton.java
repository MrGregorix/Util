package me.mrgregorix.util;

import org.apache.commons.lang.Validate;

/**
 * Represents never changing value
 *
 * @param <T> value type
 */
public final class Singleton<T>
{
    private boolean defined;
    private T value;

    /**
     * Set the singleton, this method can be invoked only once
     *
     * @param value value
     */
    public void set(final T value)
    {
        Validate.isTrue(!this.defined, "Cannot redefine singleton");
        this.defined = true;
        this.value = value;
    }

    /**
     * @return true if singleton defined, false if not
     */
    public boolean isDefined()
    {
        return defined;
    }

    /**
     * @return singleton value
     */
    public T get()
    {
        Validate.isTrue(this.defined, "Singleton not defined");
        return value;
    }
}

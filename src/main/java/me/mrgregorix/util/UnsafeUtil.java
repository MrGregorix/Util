package me.mrgregorix.util;

import java.lang.reflect.Constructor;

import sun.misc.Unsafe;

public final class UnsafeUtil
{
    private static final Unsafe unsafe;

    static
    {
        try
        {
            final Constructor<Unsafe> unsafeConstructor = Unsafe.class.getDeclaredConstructor();
            unsafeConstructor.setAccessible(true);
            unsafe = unsafeConstructor.newInstance();
        }
        catch (final Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    public static <T> T safeInstantiate(final Class<T> clazz)
    {
        try
        {
            //noinspection unchecked
            return (T) unsafe.allocateInstance(clazz);
        }
        catch (final InstantiationException e)
        {
            return null;
        }
    }

    public static Unsafe getUnsafe()
    {
        return unsafe;
    }
}
package me.mrgregorix.util.math;

/**
 * Provided parsing method for java numbers
 */
@SuppressWarnings("Duplicates")
public final class Parse
{
    private Parse()
    {
    }

    /**
     * Parses given string, returns null if string isn't valid number
     *
     * @param value string to parse
     *
     * @return number or null when invalid
     */
    public static Byte parseByte(final String value)
    {
        return parseByte(value, 10);
    }

    /**
     * Parses given string, returns null if string isn't valid number
     *
     * @param value string to parse
     * @param radix radix used while parsing
     *
     * @return number or null when invalid
     */
    public static Byte parseByte(final String value, final int radix)
    {
        if (value == null || value.length() == 0 || value.length() > 4)
        {
            return null;
        }

        Integer output = parseInt(value, radix);
        return output == null || output > Byte.MAX_VALUE || output < Byte.MIN_VALUE ? null : output.byteValue();
    }


    /**
     * Parses given string, returns null if string isn't valid number
     *
     * @param value string to parse
     *
     * @return number or null when invalid
     */
    public static Short parseShort(final String value)
    {
        return parseShort(value, 10);
    }

    /**
     * Parses given string, returns null if string isn't valid number
     *
     * @param value string to parse
     * @param radix radix used while parsing
     *
     * @return number or null when invalid
     */
    public static Short parseShort(final String value, final int radix)
    {
        if (value == null || value.length() == 0 || value.length() > 6)
        {
            return null;
        }

        Integer output = parseInt(value, radix);
        return output == null || output > Short.MAX_VALUE || output < Short.MIN_VALUE ? null : output.shortValue();
    }


    /**
     * Parses given string, returns null if string isn't valid int
     *
     * @param value string to parse
     *
     * @return int or null when invalid
     */
    public static Integer parseInt(final String value)
    {
        return parseInt(value, 10);
    }

    /**
     * Parses given string, returns null if string isn't valid int
     *
     * @param value string to parse
     * @param radix radix used while parsing
     *
     * @return int or null when invalid
     */
    public static Integer parseInt(final String value, final int radix)
    {
        if (value == null || value.length() == 0 || value.length() > 11 || radix < Character.MIN_RADIX || radix > Character.MAX_RADIX)
        {
            return null;
        }

        int limit = -Integer.MAX_VALUE;
        int result = 0;
        boolean negative = false;
        int i = 0;
        int length = value.length();
        int multmin;
        int digit;

        char firstChar = value.charAt(0);
        if (firstChar < '0')
        {
            if (firstChar == '-')
            {
                negative = true;
                limit = Integer.MIN_VALUE;
            }
            else if (firstChar != '+')
            {
                return null;
            }

            if (length == 1)
            {
                return null;
            }
            i++;
        }
        multmin = limit / radix;
        while (i < length)
        {
            digit = Character.digit(value.charAt(i++), radix);
            if (digit < 0)
            {
                return null;
            }
            if (result < multmin)
            {
                return null;
            }
            result *= radix;
            if (result < limit + digit)
            {
                return null;
            }
            result -= digit;
        }
        return negative ? result : -result;
    }

    /**
     * Parses given string, returns null if string isn't valid int
     *
     * @param value string to parse
     *
     * @return int or null when invalid
     */
    public static Long parseLong(final String value)
    {
        return parseLong(value, 10);
    }

    /**
     * Parses given string, returns null if string isn't valid int
     *
     * @param value string to parse
     * @param radix radix used while parsing
     *
     * @return int or null when invalid
     */
    public static Long parseLong(final String value, final int radix)
    {
        if (value == null || value.length() == 0 || value.length() > 20 || radix < Character.MIN_RADIX || radix > Character.MAX_RADIX)
        {
            return null;
        }

        long limit = -Long.MAX_VALUE;
        long result = 0;
        boolean negative = false;
        int i = 0;
        int length = value.length();
        long multmin;
        int digit;

        char firstChar = value.charAt(0);
        if (firstChar < '0')
        {
            if (firstChar == '-')
            {
                negative = true;
                limit = Long.MIN_VALUE;
            }
            else if (firstChar != '+')
            {
                return null;
            }

            if (length == 1)
            {
                return null;
            }
            i++;
        }
        multmin = limit / radix;
        while (i < length)
        {
            digit = Character.digit(value.charAt(i++), radix);
            if (digit < 0)
            {
                return null;
            }
            if (result < multmin)
            {
                return null;
            }
            result *= radix;
            if (result < limit + digit)
            {
                return null;
            }
            result -= digit;
        }
        return negative ? result : -result;
    }

    // TODO: Floats and doubles

    /**
     * Parses given string, returns character if string is one
     * character
     *
     * @param value string to parse
     *
     * @return char or null when invalid
     */
    public static Character parseChar(final String value)
    {
        return value != null && value.length() == 1 ? value.charAt(0) : null;
    }


    /**
     * Parses given string, returns if its value as boolean
     *
     * @param value string to parse
     *
     * @return char or null when string isn't boolean
     */
    public static Boolean parseBoolean(final String value)
    {
        if(value == null || value.length() > 5 || value.length() < 4)
        {
            return null;
        }
        if(value.equalsIgnoreCase("true"))
        {
            return true;
        }
        else if(value.equalsIgnoreCase("false"))
        {
            return false;
        }
        else
        {
            return null;
        }
    }
}

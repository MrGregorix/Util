package me.mrgregorix.util.math.operator;

/**
 * Represents an object that can be multiplied by another object
 *
 * @param <T> type of this object
 * @param <S> type of object that can be added
 */
public interface Multipliable<T extends Multipliable, S extends Multipliable>
{
    T mul(final T value1, final S value2);
}
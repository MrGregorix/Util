package me.mrgregorix.util.math.operator;

/**
 * Represents an object that can be subtracted from other object
 *
 * @param <T> type of this object
 * @param <S> type of object that can be added
 */
public interface Subtractable<T extends Subtractable, S extends Subtractable>
{
    T sub(final T value1, final S value2);
}
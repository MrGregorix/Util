package me.mrgregorix.util.math.operator;

/**
 * Represents an object that is an arithmetic value
 *
 * @param <T> type of this object
 */
public interface Arithmetic<T extends Arithmetic> extends Addable<T, T>, Subtractable<T, T>, Multipliable<T, T>, Divisible<T, T>
{
}

package me.mrgregorix.util.math.operator;

/**
 * Represents an object that can be added to other object
 *
 * @param <T> type of this object
 * @param <S> type of object that can be added
 */
public interface Addable<T extends Addable, S extends Addable>
{
    T add(final T value1, final S value2);
}
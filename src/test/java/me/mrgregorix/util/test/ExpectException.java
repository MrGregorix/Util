package me.mrgregorix.util.test;

import junit.framework.Assert;

/**
 * Fails if the exception hasn't thrown
 */
@FunctionalInterface
public interface ExpectException
{
    void run_() throws Throwable;

    default void run(final Class<? extends Throwable> expect, final String failMessage)
    {
        try
        {
            this.run_();
            Assert.fail(failMessage);
        }
        catch (final Throwable ex)
        {
            if(!expect.isInstance(ex))
            {
                Assert.fail(failMessage);
            }
        }
    }

    static void expect(final ExpectException runnable, final Class<? extends Throwable> expect, final String failMessage)
    {
        runnable.run(expect, failMessage);
    }
}

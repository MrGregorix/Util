package me.mrgregorix.util.reflect;

import org.junit.Test;

import junit.framework.Assert;

public final class ClassTest
{
    @Test
    public void testFind()
    {
        final ReflectClass<String> stringClass1 = ReflectClass.forClass(String.class);
        final ReflectClass<String> stringClass2 = ReflectClass.forObject("");
        final ReflectClass<Integer> intClass1 = ReflectClass.forClass(int.class);
        final ReflectClass<Integer> intClass2 = ReflectClass.forClass(Integer.TYPE);
        final ReflectClass<?> invalidClass1 = ReflectClass.forName("a  b  c");
        final ReflectClass<?> invalidClass2 = ReflectClass.forName("i_n_v_a_l_i_d");

        Assert.assertNotNull(stringClass1);
        Assert.assertNotNull(stringClass2);
        Assert.assertNotNull(intClass1);
        Assert.assertNotNull(intClass2);
        Assert.assertNull(invalidClass1);
        Assert.assertNull(invalidClass2);

        Assert.assertSame(stringClass1, stringClass2);
        Assert.assertSame(intClass1, intClass2);
    }
}
